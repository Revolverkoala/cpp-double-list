/*
<author>Jonathan Dennis</author>
<course>CPT-244</course>
<comment>Doubly linked list demo. I had some extra time when I wrote this, so I
thought I'd use some of it to try implementing some random fancy stuff like
iterators and move semantics.
</comment>*/

#include "doublylinkedlist.h"
#include <iostream>

using namespace std;

//namespace alias
namespace dbl = dblLinked;

//FUNCTION PROTOTYPES

void printLinkedListForward(const dbl::SimpleDoubleList& list, ostream& out);

void printLinkedListBackward(const dbl::SimpleDoubleList& list, ostream& out);

int main()
{
    //Declare variables
	const int NUMNUMS = 7;
	double initialNums [] = {45.0, 49.0, 42.0, 48.0, 52.0, 12.0, 100.0};

	//Create linked list and initialize it with the starting numbers
	dbl::SimpleDoubleList theList = dbl::SimpleDoubleList(initialNums, NUMNUMS);


	//print the numbers forward
	cout << "Now printing the initial list forward:\n\n";
	printLinkedListForward(theList, cout);
	cout << endl;

	//print the numbers backward
	cout << "Now printing the initial list backward:\n\n";
	printLinkedListBackward(theList, cout);
	cout << endl;

	//Insert 49.0 a second time and print
	theList.orderedInsert(49.0);

	//print the second list forward
	cout << "Now printing the list forward, with 49.0 added a second time:\n\n";
	printLinkedListForward(theList, cout);
	cout << endl;

	//print the second list backward
	cout << "Now printing the list backward, with 49.0 added a second time:\n\n";
	printLinkedListBackward(theList, cout);
	cout << endl;

	//Remove 42.0, 49.0, 100.0, and 52.0. Won't change the order of the rest of the list.
	double removeList[] = {42.0, 49.0, 100.0, 52.0};

	for (double& i: removeList) //C++ 11 foreach loop
	{
		theList.removeValue(i);
	}	//END for


	//Print the final list forward
	cout << "Now printing the final list forward:\n\n";
	printLinkedListForward(theList, cout);
	cout << endl;

	//Print the final list backward
	cout << "Now printing the final list backward:\n\n";
	printLinkedListBackward(theList, cout);

//	cout << "Now paused. Press any key to exit...";
//	cin.get();
	return 0;
}



/**
* Prints each element in the linked list in order.
*/
void printLinkedListForward(const dbl::SimpleDoubleList& list, ostream& out)
{
	//auto drops cv-qualifiers
	for (auto cursor = list.begin(); cursor != nullptr; ++cursor) //C++ 11 auto keyword
	{
		out << *cursor << endl;
	}
}

/**
* Prints each element in the linked list in order starting from the tail backward.
*/
void printLinkedListBackward(const dbl::SimpleDoubleList& list, ostream& out)
{
	//auto drops cv-qualifiers
	for (auto cursor = list.rbegin(); cursor != nullptr; ++cursor) //C++ 11 auto keyword
	{
		out << *cursor << endl;
	}
}
