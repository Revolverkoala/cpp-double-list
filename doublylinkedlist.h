/*
<author>Jonathan Dennis</author>
<course>CPT-244</course>

<comment>Ordered linked list container for my doubly linked list assignment. I adapated the iterator code from page 330 of our book.
It needs a lot of rewriting to be fully STL-compliant, and it doesn't have methods to do anything beyond what's required in
the homework prompt, but the lion's share of the linked list logic required to implement it is already present in dnode.cpp
and just needs to be added to the container class and tested. I commented out the non-const iterator methods because they
aren't useful for anything other than screwing up the ordering of the list, but they should be able to work like they're supposed to
(i.e. return a modifiable reference to the data member with the indirection operator).</comment> .*/
#ifndef SK_DOUBLY_LINKED_LIST_H
#define SK_DOUBLY_LINKED_LIST_H

#include "dnode.h"
#include <iterator>

//Namespace alias
namespace ms5 = main_savitch_5;

namespace dblLinked
{
	//NODE CLASS
	class SimpleDoubleList
	{
	public:
		friend class NodeIterator;
		friend class ConstNodeIterator;
		friend class NodeReverseIterator;
		friend class ConstNodeReverseIterator;
		//TYPEDEFS
		typedef std::size_t size_type;
		typedef ms5::dnode::value_type value_type;
		typedef NodeIterator iterator;
		typedef ConstNodeIterator const_iterator;
		typedef NodeReverseIterator reverse_iterator;
		typedef ConstNodeReverseIterator const_reverse_iterator;

		//Default constructor
		SimpleDoubleList();
		//Initialize with an array
		SimpleDoubleList(value_type vals[], int len);
		//Copy constructor
		SimpleDoubleList(const SimpleDoubleList& other);
		//Copy assignment operator
		SimpleDoubleList& operator=(const SimpleDoubleList& other);
		//Move constructor
		SimpleDoubleList(SimpleDoubleList&& other);
		//Move assignment operator
		SimpleDoubleList& operator=(SimpleDoubleList&& other);
		//Destructor
		~SimpleDoubleList();

		//Insertion
		void orderedInsert(value_type);
		//void push_back(ms5::dnode::value_type);
		//void push_front(ms5::dnode::value_type);

		//Deletion
		bool removeValue(value_type);

		//Search/get iterator
		//iterator search(ms5::dnode::value_type);
		//iterator lastItem();
		//const_iterator lastItem() const;
		//iterator begin();
		const_iterator begin() const;
		//iterator end();
		const_iterator end() const;
		//reverse_iterator rbegin();
		const_reverse_iterator rbegin() const;
		//reverse_iterator rend();
		const_reverse_iterator rend() const;

	private:
		ms5::dnode* headPtr;
		ms5::dnode* tailPtr;
	};

	//ITERATOR CLASSES
	class NodeIterator
		:public std::iterator < std::bidirectional_iterator_tag, SimpleDoubleList::value_type >
	{
	public:
		NodeIterator(const SimpleDoubleList* list, ms5::dnode* initialNode = nullptr)
			: owner(list)
			, currentNode(initialNode)
		{ }

		ms5::dnode::value_type& operator *()
		{
			return currentNode->data_field;
		}

		NodeIterator operator ++() //prefix ++
		{
			currentNode = currentNode->fore();
			return *this;
		}

		NodeIterator operator ++(int) //postfix ++
		{
			NodeIterator original(owner, currentNode);
			currentNode = currentNode->fore();
			return original;
		}

		NodeIterator operator --() //prefix --
		{
			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->tailPtr;
			else
				currentNode = currentNode->back();

			return *this;
		}

		NodeIterator operator --(int) //postfix --
		{
			NodeIterator original(owner, currentNode);

			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->tailPtr;
			else
				currentNode = currentNode->back();

			return original;
		}

		bool operator ==(const NodeIterator other) const
		{
			return currentNode == other.currentNode;
		}

		bool operator !=(const NodeIterator other) const
		{
			return currentNode != other.currentNode;
		}
	private:
		ms5::dnode* currentNode;
		const SimpleDoubleList* owner;
	};

	class ConstNodeIterator
		:public std::iterator < std::bidirectional_iterator_tag, const SimpleDoubleList::value_type >
	{
	public:
		ConstNodeIterator(const SimpleDoubleList* list, const ms5::dnode* initialNode = nullptr)
			: owner(list)
			, currentNode(initialNode)
		{}

		const ms5::dnode::value_type& operator *() const
		{
			return currentNode->data_field;
		}

		ConstNodeIterator operator ++() //prefix ++
		{
			currentNode = currentNode->fore();
			return *this;
		}

		ConstNodeIterator operator ++(int) //postfix ++
		{
			ConstNodeIterator original(owner, currentNode);
			currentNode = currentNode->fore();
			return original;
		}

		ConstNodeIterator operator --() //prefix --
		{
			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->tailPtr;
			else
				currentNode = currentNode->back();

			return *this;
		}

		ConstNodeIterator operator --(int) //postfix --
		{
			ConstNodeIterator original(owner, currentNode);

			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->tailPtr;
			else
				currentNode = currentNode->back();

			return original;
		}

		bool operator ==(const ConstNodeIterator other) const
		{
			return currentNode == other.currentNode;
		}

		bool operator !=(const ConstNodeIterator other) const
		{
			return currentNode != other.currentNode;
		}
	private:
		const ms5::dnode* currentNode;
		const SimpleDoubleList* owner;
	};

	class NodeReverseIterator
		:public std::iterator < std::bidirectional_iterator_tag, SimpleDoubleList::value_type >
	{
	public:
		NodeReverseIterator(const SimpleDoubleList* list, ms5::dnode* initialNode = nullptr)
			: owner(list)
			, currentNode(initialNode)
		{ }

		ms5::dnode::value_type& operator *()
		{
			return currentNode->data_field;
		}

		NodeReverseIterator operator ++() //prefix ++
		{
			currentNode = currentNode->back();
			return *this;
		}

		NodeReverseIterator operator ++(int) //postfix ++
		{
			NodeReverseIterator original(owner, currentNode);
			currentNode = currentNode->back();
			return original;
		}

		NodeReverseIterator operator --() //prefix --
		{
			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->headPtr;
			else
				currentNode = currentNode->fore();

			return *this;
		}

		NodeReverseIterator operator --(int) //postfix --
		{
			NodeReverseIterator original(owner, currentNode);

			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->headPtr;
			else
				currentNode = currentNode->fore();

			return original;
		}

		bool operator ==(const NodeReverseIterator other) const
		{
			return currentNode == other.currentNode;
		}

		bool operator !=(const NodeReverseIterator other) const
		{
			return currentNode != other.currentNode;
		}
	private:
		ms5::dnode* currentNode;
		const SimpleDoubleList* owner;
	};

	class ConstNodeReverseIterator
		:public std::iterator < std::bidirectional_iterator_tag, SimpleDoubleList::value_type >
	{
	public:
		ConstNodeReverseIterator(const SimpleDoubleList* list, const ms5::dnode* initialNode = nullptr)
			: owner(list)
			, currentNode(initialNode)
		{ }

		const ms5::dnode::value_type& operator *()
		{
			return currentNode->data_field;
		}

		ConstNodeReverseIterator operator ++() //prefix ++
		{
			currentNode = currentNode->back();
			return *this;
		}

		ConstNodeReverseIterator operator ++(int) //postfix ++
		{
			ConstNodeReverseIterator original(owner, currentNode);
			currentNode = currentNode->back();
			return original;
		}

		ConstNodeReverseIterator operator --() //prefix --
		{
			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->headPtr;
			else
				currentNode = currentNode->fore();

			return *this;
		}

		ConstNodeReverseIterator operator --(int) //postfix --
		{
			ConstNodeReverseIterator original(owner, currentNode);

			//nullptr represents "after the end" so handle that case
			if (currentNode == nullptr)
				currentNode = owner->headPtr;
			else
				currentNode = currentNode->fore();

			return original;
		}

		bool operator ==(const ConstNodeReverseIterator other) const
		{
			return currentNode == other.currentNode;
		}

		bool operator !=(const ConstNodeReverseIterator other) const
		{
			return currentNode != other.currentNode;
		}
	private:
		const ms5::dnode* currentNode;
		const SimpleDoubleList* owner;
	};
}
#endif //END SK_DOUBLY_LINKED_LIST_H
