//doublylinkedlist.cpp
#include "doublylinkedlist.h"

namespace dblLinked {
	//Default constructor
	SimpleDoubleList::SimpleDoubleList()
	{
		//Create an "empty list"; dnode functions are written to handle this
		headPtr = nullptr;
		tailPtr = nullptr;
	}

	SimpleDoubleList::SimpleDoubleList(value_type vals[], int len)
	{
		headPtr = nullptr;
		tailPtr = nullptr;

		for (int i = 0; i < len; ++i)
		{
			orderedInsert(vals[i]);
		}
	}
	//Copy constructor
	SimpleDoubleList::SimpleDoubleList(const SimpleDoubleList& other)
	{
		//Copy the other list into this one
		main_savitch_5::list_copy(other.headPtr, headPtr, tailPtr);
	}

	//Copy assignment operator
	SimpleDoubleList& SimpleDoubleList::operator=(const SimpleDoubleList& other)
	{
		//Check for self-assignment
		if (&other != this)
		{
			//Deallocate this object's memory
			main_savitch_5::list_clear(headPtr, tailPtr);

			//Copy the other list to this
			main_savitch_5::list_copy(other.headPtr, headPtr, tailPtr);
		}
		return *this;
	}

	//Move constructor
	SimpleDoubleList::SimpleDoubleList(SimpleDoubleList&& other)
	{
		//First, copy the pointers from the other object
		headPtr = other.headPtr;
		tailPtr = other.tailPtr;

		//Then, set the pointers in the other object to nullptr so this object's data isn't nuked when the other object's destructor is called
		other.headPtr = nullptr;
		other.tailPtr = nullptr;
	}

	//Move assignment operator
	SimpleDoubleList& SimpleDoubleList::operator=(SimpleDoubleList&& other)
	{
		if (this != &other)
		{
			//Deallocate this object's memory
			main_savitch_5::list_clear(headPtr, tailPtr);

			//Copy the pointers from the other object
			headPtr = other.headPtr;
			tailPtr = other.tailPtr;

			//Set the other object to null to avoid data loss
			other.headPtr = nullptr;
			other.tailPtr = nullptr;
		}
		return *this;
	}

	//Destructor
	SimpleDoubleList::~SimpleDoubleList()
	{
		//Clear the list; this deallocates all nodes
		main_savitch_5::list_clear(headPtr, tailPtr);
	}

	//Insertion
	void SimpleDoubleList::orderedInsert(main_savitch_5::dnode::value_type entry)
	{
		main_savitch_5::dnode* target = headPtr;

		//If the list is empty or the first node is already greater than the new entry, insert at the head
		if ((target == nullptr) || (target->data() >= entry))
		{
			main_savitch_5::list_head_insert(headPtr, tailPtr, entry);
			return;
		}

		//Otherwise, find the first node where the value of the next node is greater than entry

		while ((target->fore() != nullptr) && (entry > target->fore()->data()))
		{
			target = target->fore();
		}

		//Now, insert the value after the target node
		main_savitch_5::list_insert_after(tailPtr, target, entry);
	}
	//void push_back(main_savitch_5::dnode::value_type);
	//void push_front(main_savitch_5::dnode::value_type);

	//Deletion
	bool SimpleDoubleList::removeValue(main_savitch_5::dnode::value_type target)
	{
		main_savitch_5::dnode* targetPtr = main_savitch_5::list_search(headPtr, target);

		//If the target isn't there, return false
		if (targetPtr == nullptr)
			return false;

		//If the target is there, remove it
		main_savitch_5::list_remove(targetPtr, headPtr, tailPtr);
		return true;
	}

	//Search/get iterator
	//NodeIterator search(main_savitch_5::dnode::value_type);

	/*
	SimpleDoubleList::iterator SimpleDoubleList::begin()
	{
		return SimpleDoubleList::iterator(this, headPtr);
	}
	*/

	SimpleDoubleList::const_iterator SimpleDoubleList::begin() const
	{
		//
		return SimpleDoubleList::const_iterator(this, headPtr);
	}

	/*
	SimpleDoubleList::iterator SimpleDoubleList::lastItem()
	{
			return SimpleDoubleList::iterator(this, tailPtr);
	}

	SimpleDoubleList::const_iterator SimpleDoubleList::lastItem() const
	{
			return SimpleDoubleList::const_iterator(tailPtr);
	}
	*/

	/*
	SimpleDoubleList::iterator SimpleDoubleList::end()
	{
		return SimpleDoubleList::iterator(this);
	}
	*/

	SimpleDoubleList::const_iterator SimpleDoubleList::end() const
	{
		return SimpleDoubleList::const_iterator(this);
	}

	//SimpleDoubleList::reverse_iterator SimpleDoubleList::rbegin() {}

	SimpleDoubleList::const_reverse_iterator SimpleDoubleList::rbegin() const
	{
		return SimpleDoubleList::const_reverse_iterator(this, tailPtr);
	}

	//SimpleDoubleList::reverse_iterator SimpleDoubleList::rend() {}

	SimpleDoubleList::const_reverse_iterator SimpleDoubleList::rend() const
	{
		return SimpleDoubleList::const_reverse_iterator(this);
	}
}
