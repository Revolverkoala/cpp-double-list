#include <cassert>
#include "dnode.h"

namespace main_savitch_5
{
	std::size_t list_length(const dnode* head_ptr)
	{
		const dnode* cursor;
		std::size_t answer;
	
	answer = 0;
	for (cursor = head_ptr; cursor != NULL; cursor = cursor->fore( ))
	    ++answer;
	
	return answer;
	}

	/*Insert a node at the beginning of the list, which may be empty. Edited from the original.
	<precondition>head_ptr and tail_ptr are the head and tail of the same linked list, respectively.</precondition>
	<postcondition>IF head_ptr was null (an empty list), tail_ptr now points to the end (i.e. the only node).
	OTHERWISE, tail_ptr is unchanged.</postcondition>*/
    void list_head_insert(dnode*& head_ptr, dnode*& tail_ptr, const dnode::value_type& entry)
	{
		if (head_ptr == nullptr)
		{
			//If head_ptr is null, the list is empty, so create a new node. If the list is empty, tail_ptr is meaningless so reassign it to the same node
			head_ptr = new dnode(entry);
			tail_ptr = head_ptr;
		}
		else
		{
			list_insert_before(head_ptr, head_ptr, entry);
			//head_ptr = new dnode(entry, head_ptr);
			//head_ptr->fore()->set_back(head_ptr);
		}
	}
	
	/*Insert a node at the end of the list, which may be empty.
	<precondition>head_ptr and tail_ptr are the head and tail of the same linked list, respectively.</precondition>
	<postcondition>IF head_ptr was null (an empty list), head_ptr now points to the beginning (i.e. the only node.)
	REGARDLESS, tail_ptr now points to the end of the list.</postcondition>*/
	void list_tail_insert(dnode*& head_ptr, dnode*& tail_ptr, const dnode::value_type& entry)
	{
		if (tail_ptr == nullptr)
			list_head_insert(head_ptr, tail_ptr, entry);
		else
			list_insert_after(tail_ptr, tail_ptr, entry);
	}

	/*Insert a node after the specified node. Edited from the original.
	<precondition>tail_ptr is the last node in a linked list.</precondition>
	<postcondition>IF inserting at the tail, tail_ptr is updated to point to the new tail node.</postcondition>*/
	void list_insert_after(dnode*& tail_ptr, dnode* previous_ptr, const dnode::value_type& entry)
	{
		//FORWARD: Forward pointer of the previous node
		//BACK: Previous node
		dnode* insert_ptr = new dnode(entry, previous_ptr->fore(), previous_ptr);

		if (previous_ptr->fore() == nullptr)
			//If the previous node was the tail, update the tail to this node
			tail_ptr = insert_ptr;
		else
			//If not, set the backward pointer on the next node to point to this one
			previous_ptr->fore()->set_back(insert_ptr);

		//Set the forward pointer on the previous node to point to this one
		previous_ptr->set_fore(insert_ptr);
	}

	/*Insert a node before the specified node.
	<precondition>head_ptr is the first node in a linked list.</precondition>
	<postcondition>IF inserting at the head, head_ptr is updated to point to the new head node.</postcondition>*/
	void list_insert_before(dnode*& head_ptr, dnode* next_ptr, const dnode::value_type& entry)
	{
		//FORWARD: Next node
		//BACK: Backward pointer of the next node
		dnode* insert_ptr = new dnode(entry, next_ptr, next_ptr->back());
		
		if (next_ptr->back() == nullptr)
			//If the next pointer was the head, update the head to this one
			head_ptr = insert_ptr;
		else
			//If not, set the backward pointer on the previous node to point to this one
			next_ptr->back()->set_fore(insert_ptr);

		//Set the backward pointer on the next node to point to this one
		next_ptr->set_back(insert_ptr);
	}

    dnode* list_search(dnode* head_ptr, const dnode::value_type& target)
	{
		dnode* cursor;

		for (cursor = head_ptr; cursor != nullptr; cursor = cursor->fore())
			if (target == cursor->data())
				return cursor;
		return nullptr;
	}

    const dnode* list_search(const dnode* head_ptr, const dnode::value_type& target)
	{
		const dnode* cursor;

		for (cursor = head_ptr; cursor != nullptr; cursor = cursor->fore())
			if (target == cursor->data())
				return cursor;
		return nullptr;
	}

    dnode* list_locate(dnode* head_ptr, std::size_t position)
	{
		dnode* cursor;
		size_t i;

		assert (0 < position);
		cursor = head_ptr;
		for (i = 1; (i < position) && (cursor != nullptr); ++i)
			cursor = cursor->fore();
		return cursor;
	}

    const dnode* list_locate(const dnode* head_ptr, std::size_t position)
	{
		const dnode* cursor;
		size_t i;

		assert (0 < position);
		cursor = head_ptr;
		for (i = 1; (i < position) && (cursor != nullptr); ++i)
			cursor = cursor->fore();
		return cursor;
	}

	/*
    void list_head_remove(dnode*& head_ptr)
	{
		dnode* remove_ptr;

		remove_ptr = head_ptr;
		head_ptr = head_ptr->fore();
		//head_ptr now points to the new head. There is no previous node, so update the link
		head_ptr->set_back(nullptr);
		delete remove_ptr;
	}
	*/

	/*Remove the node pointed to by remove_ptr from the list. <postcondition>The forward link in the previous node now points to the node after the removed one,
	and the backward link in the next node now points to the node before the removed one.</postcondition>*/
	void list_remove(dnode* remove_ptr, dnode*& head_ptr, dnode*& tail_ptr)
	{
		//If this is the head, update head_ptr to point to the next element
		if (remove_ptr->back() == nullptr)
			head_ptr = remove_ptr->fore();
		//Otherwise set the forward pointer on the previous element to point to the element after remove_ptr
		else
			remove_ptr->back()->set_fore(remove_ptr->fore());

		//If this is the tail, update tail_ptr to point to the previous element
		if (remove_ptr->fore() == nullptr)
			tail_ptr = remove_ptr->back();
		//Otherwise set the backward pointer on the next element to point to the element before remove_ptr
		else
			remove_ptr->fore()->set_back(remove_ptr->back());

		//Now the node is safely excised and we can destroy it
		delete remove_ptr;
	}

	//Modified to work with new and altered function signatures
    void list_clear(dnode*& head_ptr, dnode*& tail_ptr)
	{
		while (head_ptr != nullptr)
			list_remove(head_ptr, head_ptr, tail_ptr);
	}


    void list_copy(const dnode* source_ptr, dnode*& head_ptr, dnode*& tail_ptr)
	{
		head_ptr = nullptr;
		tail_ptr = nullptr;

		//Handle the empty list
		if (source_ptr == nullptr)
			return;

		//Make the head node for the new list and put data in it
		list_head_insert(head_ptr, tail_ptr, source_ptr->data()); //this function now maintains tail_ptr by itself
		//tail_ptr = head_ptr;

		//Copy the rest one at a time, adding onto the tail.
		source_ptr = source_ptr->fore();
		while (source_ptr != nullptr)
		{
			list_insert_after(tail_ptr, tail_ptr, source_ptr->data()); //this function now maintains tail_ptr by itself
			//tail_ptr = tail_ptr->fore();
			source_ptr = source_ptr->fore();
		}
	}
}